import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

export default class Example extends Component {
    
    constructor() {
        super();
        this.state= {
            logins:[]
        }
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/api/logins')
        .then(response=> {
            this.setState({logins:response.data});
        });
    }

    render() {
        return (
            <div className="container">
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Celular</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.logins.map(logins=> {
                                return (
                                    <tr>
                                        <td>{logins.ploNome}</td>
                                        <td>{logins.ploEmail}</td>
                                        <td>{logins.ploCelular}</td>
                                    </tr>
                                    
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

if (document.getElementById('divLogins')) {
    ReactDOM.render(<Example />, document.getElementById('divLogins'));
}
