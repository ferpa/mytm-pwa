@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Logins</div>

                <div class="card-body" id="divLogins">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection