<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('autenticacao/', function() {
    
    $authorization = $request->headers->get('Authorization');
    $token = $request->headers->get('Token');

    if($token == env('SALT_APP'))
    {
        $retorno = new \Illuminate\Http\Response(array('type'=>'success','mensagem'=>'Sucesso.'));
        $retorno->header('Content-type','application/json');
    } else {
        $retorno = new \Illuminate\Http\Response(array('type'=>'warning','mensagem'=>'Erro de autenticação.'));
        $retorno->header('Content-type','application/json');
    }    

    return $retorno;

});


Route::get('teste/', function() {
    
    $retorno = new \Illuminate\Http\Response(array('type'=>'warning','mensagem'=>'Houve um engano.'));
    $retorno->header('Content-type','application/json');

    return $retorno;
});

Route::get('logins/','PessoaLoginController@rest');

Route::namespace('Api')->prefix('protocolos')->group(function() {
    Route::get('protocolos/','ProtocoloController@rest');
});